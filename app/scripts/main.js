$( document ).ready(function() {

	$('.single-item').slick({
		dots:true
	});

	$('.single-item2').slick({
		slidesToShow: 1,
		arrows:true,
	     slidesToScroll: 1,
	     autoplay: true,
	     autoplaySpeed: 2000,
	     mobileFirst: true,
	     responsive: [
	        {
	           breakpoint: 767,
	           settings: 'unslick'
	        }
	     ]
	});


});